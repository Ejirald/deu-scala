package txt

import java.io.File
import java.nio.file.Paths

import txt.Dto.PhrasePair

import scala.io.Source

object LoaderTxt {

  def readFiles(dirPath: String): List[File] = {
    val folder = Paths.get(dirPath).toFile
    if (folder.exists() && folder.isDirectory) {
      folder.listFiles.filter(_.isFile).toList
    } else {
      List()
    }
  }

  def readFile(f: File): List[String] = {
    Source.fromFile(f).getLines
      .map(_.trim)
      .map(_.replaceAll("[\\n\\t]", ""))
      .toList
  }

  def processFile(l: List[String]): List[PhrasePair] = {
    l.map(_.split(" - "))
      .filter(_.length < 2)
      .map(PhrasePair.apply)
  }

  def fileChain(dirPath: String):List[PhrasePair] = readFiles(dirPath).map(readFile).flatMap(processFile)
}
