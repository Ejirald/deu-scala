package txt

object Dto {

  final case class PhrasePair(w: String, t: String)

  final object PhrasePair {
    def apply(wx: Array[String]): PhrasePair = PhrasePair(wx(0), wx(1))
  }


}
