
# --- !Ups

CREATE TABLE IF NOT EXISTS word (
    id BIGSERIAL,
    word varchar(50) UNIQUE NOT NULL
);
CREATE TABLE IF NOT EXISTS translate (
    id BIGSERIAL,
    word varchar(50) UNIQUE NOT NULL
);
CREATE TABLE IF NOT EXISTS relations (
    id BIGSERIAL,
    wordId BIGINT,
    translateId BIGINT
);
CREATE UNIQUE INDEX IF NOT EXISTS relations_wordId_translateId_u_idx ON relations(wordId, translateId);

INSERT INTO word (word) VALUES ('hallo');
INSERT INTO translate (word) VALUES ('привет');
INSERT INTO relations(wordId, translateId) VALUES (
  (SELECT max(id) FROM word WHERE word = 'hallo'),
  (SELECT max(id) FROM translate WHERE word = 'привет')
);

# --- !Downs

# --- !Ups

CREATE TABLE IF NOT EXISTS tokens (
    id BIGSERIAL,
    token varchar(50) UNIQUE NOT NULL,
    user BIGINT NOT NULL,
    expiration_date BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL,
    login varchar(50) UNIQUE NOT NULL,
    email varchar(50) UNIQUE NOT NULL,
    password varchar(20) UNIQUE NOT NULL,
    enabled BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS word_status (
    id BIGSERIAL,
    word BIGINT NOT NULL,
    user BIGINT NOT NULL,
    status TINYINT NOT NULL
);

# --- !Downs

# --- !Ups

CREATE TABLE IF NOT EXISTS roles (
    id BIGSERIAL,
    name varchar(50) UNIQUE NOT NULL
);
INSERT INTO roles (name) VALUES ('admin'), ('user');

CREATE TABLE IF NOT EXISTS role_to_user (
    id      BIGSERIAL,
    role_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    UNIQUE (role_id, user_id)
);

CREATE TABLE IF NOT EXISTS permissions (
    id BIGSERIAL,
    value varchar(50) UNIQUE NOT NULL
);
INSERT INTO permissions (value) VALUES ('R'), ('W');

CREATE TABLE IF NOT EXISTS permission_to_role (
    id              BIGSERIAL,
    permission_id   BIGINT NOT NULL,
    role_id         BIGINT NOT NULL,
    UNIQUE (permission_id, role_id)
);
INSERT INTO permission_to_role (role_id, permission_id)
VALUES
  (SELECT max(id) FROM roles WHERE name = 'admin', SELECT max(id) FROM permissions WHERE value = 'R'),
  (SELECT max(id) FROM roles WHERE name = 'admin', SELECT max(id) FROM permissions WHERE value = 'W'),
  (SELECT max(id) FROM roles WHERE name = 'user',  SELECT max(id) FROM permissions WHERE value = 'R');

# --- !Downs