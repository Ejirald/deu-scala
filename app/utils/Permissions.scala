package utils

import be.objectify.deadbolt.scala.ActionBuilders
import be.objectify.deadbolt.scala.models.PatternType
import play.api.libs.json.JsValue
import play.api.mvc.Results.BadRequest
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Permissions {
  def Rights(permissions: String, servlet: Request[AnyContent] => Result)
            (implicit ab: ActionBuilders, bodyParsers: PlayBodyParsers): Action[AnyContent] =
    ab.PatternAction(value = permissions, patternType = PatternType.EQUALITY)
      .defaultHandler() { request: Request[AnyContent] =>
        Future {
          servlet(request)
        }
      }

  def JRights[T](permissions: String, jServlet: JsValue => Result)
                (implicit ab: ActionBuilders, bodyParsers: PlayBodyParsers): Action[AnyContent] =
    ab.PatternAction(value = permissions, patternType = PatternType.EQUALITY).defaultHandler() {
      request: Request[AnyContent] =>
        Future {
          request.body.asJson
            .map { json => jServlet(json) }
            .getOrElse {
              BadRequest("Expecting application/json request body")
            }
        }
    }
}
