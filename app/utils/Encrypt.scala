package utils

import java.security.MessageDigest

object Encrypt {

  final def shaPassword(password: String): String = MessageDigest
    .getInstance("SHA-256")
    .digest(password.getBytes("UTF-8"))
    .toString

}
