package utils

import java.util
import java.util.Collections

object LRUCacheBuilder {
  private class LRUCache[K, V](private val cacheSize: Integer)
    extends util.LinkedHashMap[K, V](16, 0.75F, true) {

    final override protected
    def removeEldestEntry(eldest: util.Map.Entry[K, V]):Boolean =size() >= cacheSize
  }
  def build[K,V](cacheSize:Integer): util.Map[K,V] = Collections.synchronizedMap(new LRUCache[K,V](cacheSize))
}


