package controllers

import be.objectify.deadbolt.scala.ActionBuilders
import javax.inject.Inject
import models._
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.Json
import play.api.mvc._

final class Registration @Inject()(val cc: ControllerComponents,
                                   val ts: Tokens,
                                   val us: Users,
                                   val roles: Roles,
                                   implicit val ab: ActionBuilders,
                                   implicit val bodyParsers: PlayBodyParsers) extends AbstractController(cc) {

  val logger: Logger = Logger(this.getClass)

  final val userForm = Form(mapping(
    "login" -> nonEmptyText(6, 50),
    "email" -> email,
    "password" -> nonEmptyText(6, 50)
  )(Users.User.applyFormData)(Users.User.unapplyFormData))

  def register: Action[AnyContent] = Action { implicit request: Request[_] =>
    userForm.bindFromRequest.fold(
      failure => {
        BadRequest(s"Cannot create user with this credentials :: validation error")
      },
      userData => {
        us.upsert(userData) match {
          case Some(user) =>
            ts.createToken(user.id.get) match {
              case Some(token) => Ok(Json.toJson(token)).as(JSON)
              case _ => BadRequest("Cannot create token")
            }
          case None => BadRequest("User with this email or login already exists")
        }
      })
  }

  def confirm(token: String): Action[AnyContent] = Action {
    val user: Option[Users.User] = for {
      userID <- if (ts.verifyToken(token)) ts.getUserIdByToken(token) else None
      user <- {
        logger.debug(s"UserID :: $userID")
        us.getUser(userID)
      }
      newUser <- {
        logger.debug(s"User :: $user")
        us.upsert(Users.User(user.id, user.login, user.email, user.password, enabled = true))
      }
    } yield newUser

    user match {
      case Some(newUser) =>
        if (roles.addRoleMapping(newUser.id.get, "user").isFailure)
          logger.info("User to role already mapped")
        Ok(Json.toJson(newUser)).as(JSON)
      case _ => ExpectationFailed("User for this token doesn't exists")
    }
  }
}
