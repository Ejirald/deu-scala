package controllers

import be.objectify.deadbolt.scala.ActionBuilders
import javax.inject.Inject
import models.Words
import play.api.libs.json.Json
import play.api.mvc._
import utils.Permissions.{JRights, Rights}

final class WordsController @Inject()(val cc: ControllerComponents,
                                      val ws: Words,
                                      implicit val ab: ActionBuilders,
                                      implicit val bodyParsers: PlayBodyParsers) extends AbstractController(cc) {

  def insertWordToTranslates(): Handler = JRights("W", { ws.insertPair })

  def insertWord(): Handler = JRights("W", { ws.insertWord })

  def updateWord(): Handler = JRights("W", { ws.insertTranslate })

  def insertTranslate(): Handler = JRights("W", { ws.insertTranslate })

  def updateTranslate(): Handler = JRights("W", { ws.updateTranslate })

  def deleteTranslate(id: Long): Action[AnyContent] = Rights("W", _ => if (ws.deleteTranslate(id) > 0) Ok else NotFound)

  def deleteWord(id: Long): Action[AnyContent] = Rights("W", _ => if (ws.deleteWord(id) > 0) Ok else NotFound)

  def getAllWords: Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findAll)).as(JSON))

  def getAllTranslates: Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findAllTranslates)).as(JSON))

  def getWordsWithTranslates: Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findWordsWithTranslates)).as(JSON))

  def findWordById(id: Long): Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findWordById(id))).as(JSON))

  def findTranslateById(id: Long): Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findTranslateById(id))).as(JSON))

  def findWordTranslatesById(id: Long): Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findWordTranslatesById(id))).as(JSON))

  def findTranslateWordsById(id: Long): Action[AnyContent] = Rights("R", _ => Ok(Json.toJson(ws.findTranslateWordsById(id))).as(JSON))
}
