package controllers

import be.objectify.deadbolt.scala.ActionBuilders
import javax.inject.Inject
import models._
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.Json
import play.api.mvc._

final case class UserData(login: String, password: String)

final class Auth @Inject()(val cc: ControllerComponents,
                           val ts: Tokens,
                           val us: Users,
                           implicit val ab: ActionBuilders,
                           implicit val bodyParsers: PlayBodyParsers) extends AbstractController(cc) {
  val logger: Logger = Logger(this.getClass)

  val userForm: Form[UserData] = Form(mapping(
    "login" -> text,
    "password" -> text
  )(UserData.apply)(UserData.unapply))

  def getTokenByUser = Action { implicit request: Request[_] =>
    userForm.bindFromRequest.fold(
      failure => {
        BadRequest(s"Cannot find user with this credentials")
      },
      userData => {
        val maybeToken: Option[Tokens.VerificationToken] = for {
          user: Users.User <- {
            logger.info(s"User data :: [$userData]")
            us.getUserByEmailOrLogin(userData.login)
          }
          userID: Long <- {
            logger.info(s"User :: [$user]")
            user.id
          }
          token: Tokens.VerificationToken <- {
            logger.info(s"User ID :: [$userID]")
            ts.getTokenByUserId(userID)
          }
        } yield token

        maybeToken match {
          case Some(token) =>
            logger.info(s"Token :: [$token]")
            Ok(Json.toJson(token)).as(JSON)
          case _ => ExpectationFailed("Token for this user doesn't exists")
        }
      }
    )
  }
}
