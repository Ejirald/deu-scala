package controllers

import javax.inject._
import play.api.routing._
import play.api.routing.sird._

final class WordsRouter @Inject()(wc: WordsController) extends SimpleRouter {
  override def routes: Router.Routes = {
    case GET(p"/all") => wc.getAllWords
    case GET(p"/allWithTranslates") => wc.getWordsWithTranslates

    case POST(p"/") => wc.insertWord()
    case GET(p"/${long(id)}") => wc.findWordById(id)
    case GET(p"/translates/${long(id)}") => wc.findWordTranslatesById(id)
    case PUT(p"/") => wc.updateWord()
    case DELETE(p"/${long(id)}") => wc.deleteWord(id)

    case POST(p"/translate/") => wc.insertTranslate()
    case GET(p"/translate/${long(id)}") => wc.findTranslateById(id)
    case GET(p"/translate/all") => wc.getAllTranslates()
    case GET(p"/translate/words/${long(id)}") => wc.findTranslateWordsById(id)
    case PUT(p"/translate/") => wc.updateTranslate()
    case DELETE(p"/translate/${long(id)}") => wc.deleteTranslate(id)

    case POST(p"/withTranslates") => wc.insertWordToTranslates()
  }
}