package auth

import java.util

import be.objectify.deadbolt.scala.models.Subject
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler, DynamicResourceHandler}
import models.Subjects
import play.api.http.MimeTypes._
import play.api.mvc.{Request, Result, Results}
import utils.LRUCacheBuilder

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Implementation for deadbolt authorization handler
  *
  * @param subjects contains token, roles & permissions for registered & auth user
  */
final class HandlerImpl(private val subjects: Subjects) extends DeadboltHandler {

  val users: util.Map[String, Subject] = LRUCacheBuilder.build[String, Subject](100)

  override
  def beforeAuthCheck[A](request: Request[A]): Future[Option[Result]] = Future {
    None
  }

  override
  def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = Future {
    None
  }

  override
  def getSubject[A](request: AuthenticatedRequest[A]): Future[Option[Subject]] = Future {
    request.subject.orElse {
      request.headers.get("userToken") match {
        case Some(token) =>
          if (users.containsKey(token))
            Some(users.get(token))
          else
            subjects.getSubjectByToken(token).map(it => users.put(token, it))
        case _ => None
      }
    }
  }

  override
  def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = Future {
    Results.Forbidden("").as(HTML)
  }
}