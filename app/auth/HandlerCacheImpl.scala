package auth

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{DeadboltHandler, HandlerKey}
import javax.inject.{Inject, Singleton}
import models.Subjects

final case class Key(name: String) extends HandlerKey

@Singleton
final class HandlerCacheImpl @Inject()(private val subjects: Subjects) extends HandlerCache {

  val defaultHandler: DeadboltHandler = new HandlerImpl(subjects)

  val handlers: Map[HandlerKey, DeadboltHandler] = Map(Key("default") -> defaultHandler)

  override def apply(): DeadboltHandler = defaultHandler

  override def apply(handlerKey: HandlerKey): DeadboltHandler = handlers(handlerKey)
}