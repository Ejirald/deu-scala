package models

import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}
import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.collection.immutable

class Subjects @Inject()(protected val permissionsService: Permissions,
                         protected val rolesService: Roles,
                         protected val tokenService: Tokens,
                         protected val dbConfigProvider: DatabaseConfigProvider,
                         private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {

  def getSubjectByToken(token: String): Option[Subject] = {
    val maybeUserID: Option[Long] = tokenService.getUserIdByToken(token)
    if (maybeUserID.isEmpty) return None

    val _roles: immutable.Seq[Roles.RoleImpl] = rolesService.rolesByUserId(maybeUserID.get)
    if (_roles.isEmpty) return None

    val _permissions: Seq[Permission] = permissionsService.permByRoleId(_roles.map(_.id).filter(_.isDefined).map(_.get).toList)
    if (_permissions.isEmpty) return None

    Some(new Subject {
      override def identifier: String = token

      override def roles: List[Role] = _roles.toList

      override def permissions: List[Permission] = _permissions.toList
    })
  }
}
