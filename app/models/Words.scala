package models

import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json._
import play.api.mvc.Result
import play.api.mvc.Results.{BadRequest, InternalServerError, Ok}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcProfile
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction, SqlStreamingAction}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Words {

  // DTO

  final case class Word(id: Option[Long], word: String)

  final case class Relation(id: Option[Long], wordId: Long, translateId: Long)

  final case class WordToTranslates(word: String, translates: Seq[String])

  // Implicit mappers

  implicit val wordReads: Reads[Word] = Json.reads[Word]
  implicit val wordWrites: OWrites[Word] = Json.writes[Word]

  implicit val wordToTranslatesReads: Reads[WordToTranslates] = Json.reads[WordToTranslates]
  implicit val wordToTranslatesWrites: OWrites[WordToTranslates] = Json.writes[WordToTranslates]

  // DAO

  final class RelationsDao(tag: Tag) extends Table[Relation](tag, "relations") {
    def id = column[Option[Long]]("id", O.PrimaryKey)

    def wordId = column[Long]("wordId")

    def translateId = column[Long]("translateId")

    def * = (id, wordId, translateId).mapTo[Relation]
  }

  class WordsDao(tag: Tag, table: String = "word") extends Table[Word](tag, table) {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def word = column[String]("word", O.Unique)

    def * = (id, word).mapTo[Word]
  }

  // Repository

  final class TranslatesDao(tag: Tag, table: String = "translate") extends WordsDao(tag, table)

  private lazy val _messages = TableQuery[WordsDao]
  private lazy val _translates = TableQuery[TranslatesDao]
  private implicit val relations: TableQuery[RelationsDao] = TableQuery[RelationsDao]

  final class Queries[T <: WordsDao, A <: WordsDao](val mainTQ: TableQuery[T],
                                                    val partnerTQ: TableQuery[A])
                                                   (implicit mapping: TableQuery[RelationsDao])
    extends TableQuery(_ => mainTQ.baseTableRow) {

    def apply(mainTable: TableQuery[T],
              partnerTable: TableQuery[T],
              mapping: TableQuery[RelationsDao]): Queries[T, A] = new Queries(mainTQ, partnerTQ)(mapping)

    def findById(id: Long): FixedSqlStreamingAction[Seq[Word], Word, Effect.Read] =
      mainTQ.filter(_.id === id).result

    def findByWord(word: String): FixedSqlStreamingAction[Seq[Word], Word, Effect.Read] =
      mainTQ.filter(_.word === word).result

    def findTranslatesById(id: Long): FixedSqlStreamingAction[Seq[Word], Word, Effect.Read] =
      mapping join mainTQ on (_.wordId === _.id) join partnerTQ on (_._1.translateId === _.id) map (_._2) result

    def findWordsWithTranslates: SqlStreamingAction[Vector[(String, Option[String])], (String, Option[String]), Effect] = sql"""
           SELECT w.word, t.word
           FROM relations AS rl
           LEFT JOIN word AS w ON rl.wordId = w.id
           LEFT JOIN translate AS t ON rl.translateId = t.id
      """.as[(String, Option[String])]
  }

  lazy val messages: Queries[WordsDao, TranslatesDao] = new Queries(_messages, _translates)
  lazy val translates: Queries[TranslatesDao, WordsDao] = new Queries(_translates, _messages)
}

final class Words @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                            private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {

  import Words._

  // Util methods

  def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  // Service methods

  def upsert[A <: WordsDao, B <: WordsDao](word: JsValue,
                                           messages: Queries[A, B],
                                           function: (Queries[A, B], Word) => FixedSqlAction[Int, NoStream, Effect.Write]
                                          ): Result =
    word.validate[Word] match {
      case e: JsError => System.err.println(s"$e :: $word"); BadRequest
      case JsSuccess(w: Word, _: JsPath) => await(function(messages, w).asTry) match {
        case Success(count: Int) => Ok(count toString)
        case Failure(ex) => System.err.println(ex); InternalServerError
      }
    }

  def insertWord(word: JsValue): Result = upsert(word, messages, (queries: Queries[WordsDao, TranslatesDao], w) => queries += w)

  def updateWord(word: JsValue): Result = upsert(word, messages, (queries: Queries[WordsDao, TranslatesDao], w) => queries.filter(_.id === w.id).update(w))

  def insertTranslate(word: JsValue): Result = upsert(word, translates, (queries: Queries[TranslatesDao, WordsDao], w) => queries += w)

  def updateTranslate(word: JsValue): Result = upsert(word, translates, (queries: Queries[TranslatesDao, WordsDao], w) => queries.filter(_.id === w.id).update(w))

  def insertPair(wordToTranslates: JsValue): Result = wordToTranslates.validate[WordToTranslates] match {
    case e: JsError =>
      System.err.println(s"Insert pait error :: $e :: $wordToTranslates")
      BadRequest
    case JsSuccess(w: WordToTranslates, _: JsPath) =>
      val wid: Option[Long] = await(messages.filter(_.word === w.word).map(_.id).result) match {
        case Seq(ids) => Some(ids.head)
        case Seq() => await(messages returning messages.map(_.id) += Word(None, w.word))
      }

      val tids: Seq[Long] = {
        val translateExists: Seq[Word] = await(translates.filter(_.word inSet w.translates).result)
        val notInDb: Set[String] = w.translates.toSet -- translateExists.map(_.word).toSet
        val translatesNew: Seq[Long] = await(translates returning translates.map(_.id.get) ++= notInDb.map(it => Word(None, it)))

        translateExists.map(_.id.get).toSet ++ translatesNew.toSet toSeq
      }

      if (wid.isEmpty || tids.isEmpty) {
        System.err.println(s"Invalid data to insert :: $wid :: $tids")
        return Ok
      }

      val rels: Seq[Relation] = tids.map(tid => Relation(None, wid.get, tid))
      rels.map(it => await((relations += it).asTry) match {
        case Success(count: Int) => count
        case Failure(ex) =>
          System.err.println(ex)
          0
      }).sum match {
        case 0 => InternalServerError
        case sum: Int => Ok(sum toString)
      }
  }

  def findAll: Seq[Word] = await(messages.result)

  def findAllTranslates: Seq[Word] = await(translates.result)

  def findWordById(id: Long): Word = await(messages.findById(id)).head

  def findTranslateById(id: Long): Word = await(translates.findById(id)).head

  def findWordTranslatesById(id: Long): Seq[Word] = await(messages.findTranslatesById(id))

  def findTranslateWordsById(id: Long): Seq[Word] = await(translates.findTranslatesById(id))

  def findWordTranslatesStrById(id: Long): Seq[String] = findWordTranslatesById(id).map(_.word)

  def findTranslateWordsStrById(id: Long): Seq[String] = findTranslateWordsById(id).map(_.word)

  def findWordsWithTranslates: Seq[WordToTranslates] = await(messages.findWordsWithTranslates)
    .groupBy(_._1)
    .map(it => WordToTranslates(it._1, it._2.map(_._2.getOrElse("-|-"))))
    .toSeq

  def deleteWord(id: Long): Int = Await.result(db.run(messages.filter(_.id === id).delete), 2 second)

  def deleteTranslate(id: Long): Int = Await.result(db.run(translates.filter(_.id === id).delete), 2 second)

}