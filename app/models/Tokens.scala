package models

import java.util.UUID

import javax.inject.Inject
import models.Tokens.VerificationToken
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Json, OWrites, Reads}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcProfile
import slick.sql.{FixedSqlAction, SqlAction}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Tokens {

  final case class VerificationToken(id: Option[Long], token: String, user: Long, expirationDate: Long) {
    def idValid: Boolean = expirationDate >= System.currentTimeMillis()
  }

  implicit val reads: Reads[VerificationToken] = Json.reads[VerificationToken]
  implicit val writes: OWrites[VerificationToken] = Json.writes[VerificationToken]

  final class VerificationTokenDao(tag: Tag) extends Table[VerificationToken](tag, "tokens") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def token = column[String]("token")

    def user = column[Long]("user")

    def expirationDate = column[Long]("expiration_date")

    override def * = (id, token, user, expirationDate).mapTo[VerificationToken]
  }

  lazy val tokens = TableQuery[VerificationTokenDao]

  def createToken(token: VerificationToken): FixedSqlAction[Option[Long], NoStream, Effect.Write] =
    (tokens returning tokens.map(_.id)) += token

  def getTokenByToken(token: String): SqlAction[Option[VerificationToken], NoStream, Effect.Read] =
    tokens.filter(_.token === token).take(1).result.headOption

  def updateToken(token: VerificationToken): FixedSqlAction[Int, NoStream, Effect.Write] =
    tokens.update(token)

  def getUserIdByToken(token: String): SqlAction[Option[Long], NoStream, Effect.Read] =
    tokens.filter(_.token === token).map(_.user).result.headOption

  def getTokenByUserId(id: Long): SqlAction[Option[VerificationToken], NoStream, Effect.Read] =
    tokens.filter(_.user === id).result.headOption
}

final class Tokens @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                             private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {
  // Util methods

  private def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  private def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  // Service methods

  def createToken(userID: Long): Option[Tokens.VerificationToken] = {
    val vt = VerificationToken(None, UUID.randomUUID().toString, userID, System.currentTimeMillis() + 3600 * 24 * 1000)
    await(Tokens.createToken(vt).asTry) match {
      case Success(id) => Some(VerificationToken(id, vt.token, vt.user, vt.expirationDate))
      case Failure(reason) => println(reason.getMessage); None
    }
  }

  def verifyToken(token: String): Boolean = {
    await(Tokens.getTokenByToken(token)) match {
      case None => false
      case Some(savedToken) =>
        val expirationDate = System.currentTimeMillis()
        val result: Boolean = savedToken.expirationDate > expirationDate
        if (result) {
          Tokens.updateToken(Tokens.VerificationToken(savedToken.id, savedToken.token, savedToken.user, expirationDate))
        }
        result
    }
  }

  def getUserIdByToken(token: String): Option[Long] = await(Tokens.getUserIdByToken(token))

  def getTokenByUserId(id: Long): Option[VerificationToken] = {
    await(Tokens.getTokenByUserId(id)) match {
      case ot: Option[VerificationToken] => ot
      case _ => createToken(id)
    }
  }
}
