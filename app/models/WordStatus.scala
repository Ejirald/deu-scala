package models

import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.{GetResult, JdbcProfile}
import slick.sql.{FixedSqlStreamingAction, SqlStreamingAction}

import scala.concurrent.Await
import scala.concurrent.duration._

object WordStatus {

  // DTO

  final case class Status(id: Option[Long], word: Long, user: Long, status: Byte)

  // DAO

  final class StatusDao(tag: Tag) extends Table[Status](tag, "word_status") {
    def id = column[Option[Long]]("id", O.PrimaryKey)

    def word = column[Long]("word")

    def user = column[Long]("user")

    def status = column[Byte]("status")

    def * = (id, word, user, status).mapTo[Status]
  }

  // Repository

  lazy val statuses = TableQuery[StatusDao]

  implicit val getStatus: AnyRef with GetResult[Status] = GetResult[Status](r => Status(r.nextLongOption(), r.nextLong(), r.nextLong(), r.nextByte()))

  def getByUser(user: Long): FixedSqlStreamingAction[Seq[Status], Status, Effect.Read] = statuses.filter(_.user === user).result

  def getWordByUser(user: Long, word: Long): FixedSqlStreamingAction[Seq[Status], Status, Effect.Read] = statuses.filter(_.word === word).result

  def getWordByUser(user: String, word: String): SqlStreamingAction[Vector[Status], Status, Effect] = sql"""
         SELECT ws.* FROM word_status AS ws WHERE user = (
          SELECT max(id) FROM users WHERE login = $user OR email = $user
         ) AND word = (
          SELECT max(id) FROM words WHERE word = $word
         )
       """.as[Status]
}

final class WordStatus @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                 private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {
  // Util methods

  def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  // Service methods

  def getByUser(id: Long): Seq[WordStatus.Status] = await(WordStatus.getByUser(id))

  def getWordByUser(user: Long, word: Long): Seq[WordStatus.Status] = await(WordStatus.getWordByUser(user, word))

  def getWordByUser(user: String, word: String): Vector[WordStatus.Status] = await(WordStatus.getWordByUser(user, word))
}
