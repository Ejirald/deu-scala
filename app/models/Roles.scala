package models

import be.objectify.deadbolt.scala.models.Role
import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Json, OWrites, Reads}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.{GetResult, JdbcProfile}
import slick.sql.{SqlAction, SqlStreamingAction}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}
import scala.util.Try

object Roles {

  final case class RoleImpl(id: Option[Long], name: String) extends Role

  implicit def stringToRole(s: String): Role = RoleImpl(None, s)

  implicit val roleReads: Reads[RoleImpl] = Json.reads[RoleImpl]
  implicit val roleWrites: OWrites[RoleImpl] = Json.writes[RoleImpl]

  final class RoleDao(tag: Tag) extends Table[RoleImpl](tag, "roles") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    override def * = (id, name).mapTo[RoleImpl]
  }

  private lazy val roles = TableQuery[RoleDao]

  private implicit val getResult: AnyRef with GetResult[RoleImpl] = GetResult[RoleImpl](r => RoleImpl(r.nextLongOption(), r.nextString()))

  private def roleByUserId(user_id: Long): SqlStreamingAction[Vector[RoleImpl], RoleImpl, Effect] = sql"""
           SELECT r.* FROM roles AS r
             JOIN role_to_user AS ru ON r.id = ru.role_id AND ru.user_id = $user_id
      """.as[RoleImpl]

  private def addRoleMapping(user_id: Long, role: String): SqlAction[Int, NoStream, Effect] =
    sqlu"""
         INSERT INTO role_to_user (user_id, role_id) VALUES ($user_id, SELECT max(id) FROM roles WHERE name = $role)
       """
}

class Roles @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                      private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {

  def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  def rolesByUserId(id: Long): Vector[Roles.RoleImpl] = await(Roles.roleByUserId(id))

  def addRoleMapping(userID: Long, role: String): Try[Int] = await(Roles.addRoleMapping(userID, role).asTry)
}
