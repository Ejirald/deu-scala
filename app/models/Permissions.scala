package models

import be.objectify.deadbolt.scala.models.Permission
import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Json, OWrites, Reads}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.{GetResult, JdbcProfile}
import slick.lifted.ProvenShape
import slick.sql.SqlStreamingAction

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}

object Permissions {

  final case class PermImpl(id: Option[Long], value: String) extends Permission

  implicit def stringToPerm(s: String): Permission = PermImpl(None, s)

  implicit val permReads: Reads[PermImpl] = Json.reads[PermImpl]
  implicit val permWrites: OWrites[PermImpl] = Json.writes[PermImpl]

  final class PermDao(tag: Tag) extends Table[PermImpl](tag, "permissions") {
    def id: Rep[Option[Long]] = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def value: Rep[String] = column[String]("value")

    override def * : ProvenShape[PermImpl] = (id, value).mapTo[PermImpl]
  }

  private lazy val permissions = TableQuery[PermDao]

  private implicit val getPermImpl: AnyRef with GetResult[PermImpl] = GetResult[PermImpl](r => PermImpl(r.nextLongOption(), r.nextString()))

  private def permByRoleId(role_ids: List[Long]): SqlStreamingAction[Vector[PermImpl], PermImpl, Effect] = sql"""
           SELECT p.* FROM permissions AS p
             JOIN permission_to_role AS pr ON p.id = pr.permission_id AND pr.role_id IN (${role_ids.mkString})
      """.as[PermImpl]
}

class Permissions @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                            private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {

  def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  def permByRoleId(ids: List[Long]): Seq[Permission] = await(Permissions.permByRoleId(ids))
}
