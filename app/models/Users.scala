package models

import javax.inject.Inject
import models.Users.User
import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Json, OWrites, Reads}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcProfile
import slick.sql.{FixedSqlAction, SqlAction}
import utils.Encrypt

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Users {

  case class User(id: Option[Long], login: String, email: String, password: String, enabled: Boolean)

  object User {
    def map(id: Option[Long], user: User): User = new User(id, user.login, user.email, user.password, user.enabled)

    def apply(id: Option[Long], login: String, email: String, password: String, enabled: Boolean): User = new User(id, login, email, password, enabled)

    def unapply(user: User): Option[(Option[Long], String, String, String, Boolean)] = Some(user.id, user.login, user.email, user.password, user.enabled)

    def applyFormData(login: String, email: String, password: String): User = User(None, login, email, Encrypt.shaPassword(password), enabled = false)

    def unapplyFormData(user: User): Option[(String, String, String)] = Some(user.login, user.email, user.password)
  }

  implicit val userReads: Reads[User] = Json.reads[User]
  implicit val userWrites: OWrites[User] = (user: User) => Json.obj(
    "id" -> user.id,
    "login" -> user.login,
    "email" -> user.email,
    "enabled" -> user.enabled
  )

  // DAO

  final class UserDao(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def login = column[String]("login")

    def email = column[String]("email")

    def password = column[String]("password")

    def enabled = column[Boolean]("enabled")

    override def * = (id, login, email, password, enabled) <> ((User.apply _).tupled, User.unapply)
  }

  // Repository

  lazy val users: Query[UserDao, User, Seq] = TableQuery[UserDao]

  def getUser(id: Long): SqlAction[Option[User], NoStream, Effect.Read] = users.filter(_.id === id).result.headOption

  def getUserByEmail(email: String): SqlAction[Option[User], NoStream, Effect.Read] = users.filter(_.email === email).take(1).result.headOption

  def getUserByLogin(login: String): SqlAction[Option[User], NoStream, Effect.Read] = users.filter(_.login === login).take(1).result.headOption

  def upsert(user: User): FixedSqlAction[Option[Long], NoStream, Effect.Write] = (users returning users.map(_.id.get)) insertOrUpdate user
}

final class Users @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                            private val configuration: play.api.Configuration) extends HasDatabaseConfigProvider[JdbcProfile] {

  final val logger = Logger(this.getClass)

  // Util methods

  private def awaitTime[R](query: DBIOAction[R, NoStream, Nothing], time: Duration): R = Await.result(db.run(query), time)

  private def await[R](query: DBIOAction[R, NoStream, Nothing]): R = awaitTime(query, configuration.get[Long]("timeout") second)

  // Service methods

  def getUser(id: Long): Option[Users.User] = await(Users.getUser(id))

  def getUserByEmail(email: String): Option[Users.User] = await(Users.getUserByEmail(email))

  def getUserByLogin(login: String): Option[Users.User] = await(Users.getUserByLogin(login))

  def getUserByEmailOrLogin(login: String): Option[Users.User] = getUserByEmail(login) match {
    case None => getUserByLogin(login)
    case o: Option[Users.User] => o
  }

  def upsert(user: User): Option[User] = {
    val sql = Users.upsert(user)
    await(sql.asTry) match {
      case Failure(reason) => println("reason :: " + reason.getMessage); None
      case Success(id) =>
        logger.debug(s"Upsert user with [$id]/[${user.id}] ID")
        if (user.id.isDefined)
          Some(User(user.id, user.login, user.email, user.password, user.enabled))
        else
          Some(User(id, user.login, user.email, user.password, user.enabled))
    }
  }
}
