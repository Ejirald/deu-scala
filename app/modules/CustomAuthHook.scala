package modules

import auth.HandlerCacheImpl
import be.objectify.deadbolt.scala.cache.HandlerCache
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

/**
  * Bindings from additional interfaces to local-realization classes
  */
final class CustomAuthHook extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[HandlerCache].to[HandlerCacheImpl]
  )
}