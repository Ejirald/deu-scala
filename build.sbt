
lazy val circeVersion = "0.10.0"
lazy val _version = "0.1"

name := "deutch-wortebuch"
organization in ThisBuild := "com.mayur"
scalaVersion in ThisBuild := "2.12.6"
version in ThisBuild := _version

lazy val global = project
  .in(file("."))
  .settings(
    name := "wortebuch",
    organization := "com.mayur",
    scalaVersion := "2.12.6",
    version := "0.0.2",

    settings,

    libraryDependencies ++= Seq(
      guice,
      "org.slf4j" % "slf4j-api" % "1.7.7",
      "com.h2database" % "h2" % "1.4.196",

      "com.typesafe.play" %% "play-slick" % "3.0.0",
      "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",

      "be.objectify" %% "deadbolt-scala" % "2.6.0",

      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion

    ),

    PlayKeys.externalizeResources := false
  ).aggregate(loaders).enablePlugins(PlayScala)

lazy val loaders = project
  .settings(
    name := "loaders",
    version := _version,
    settings,
    scalacOptions += "-Ypartial-unification"
  )

// ----------------------------------------------
// ---------------- Settings --------------------
// ----------------------------------------------

lazy val settings = scalacOptions ++= Seq(
  "-unchecked",
  "-feature",
  "-language:implicitConversions",
  "-language:reflectiveCalls",
  "-language:existentials",
  "-language:higherKinds",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

resolvers += Resolver.sbtPluginRepo("releases")